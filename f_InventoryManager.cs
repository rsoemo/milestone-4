﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone_4
{
    public partial class f_InventoryManager : Form
    {
        //=============================//
        //=======Class=Variables=======//
        //=============================//
        int currentPage = 1;

        //=========================//
        //=======Constructor=======//
        //=========================//
        public f_InventoryManager()
        {
            InitializeComponent();
        }

        //==============================//
        //=======When=Form=Loaded=======//
        //==============================//
        private void f_InventoryManager_Load(object sender, EventArgs e)
        {
            Program.pageList = Program.inventory.pullForMenu(currentPage);  //Pulling Page
            printMenuPage(Program.pageList);  //Printing Page
            Program.inventory.generatePageCount();  //Generating Page Number
            currentPage = 1;
            setPagesCountText();
        }

        //==================================//
        //=======Set=Pages=Count=Text=======//
        //==================================//
        public void setPagesCountText()
        {
            tl_Pages.Text = currentPage.ToString() + "/" + Program.inventory.getPageCount().ToString();  //Printing Page Number
        }

        //=============================//
        //=======Print=Menu=Page=======//
        //=============================//
        private void printMenuPage(List<InventoryItem> pageItems)
        {
            //tl_ItemStock1.Text = pageItems[0].getItemStock().ToString();  //Stock

            //===Adding=Item=One===//
            tl_ItemName1.Text = pageItems[0].getItemName();  //Name
            tl_ItemPrice1.Text = "$" + pageItems[0].getItemPrice().ToString();  //Price
            nud_ItemStock1.Value = pageItems[0].getItemStock();  //Stock
            tl_ItemWeight1.Text = pageItems[0].getItemWeight().ToString();  //Weight
            tl_ItemSize1.Text = pageItems[0].getItemSize().ToString();  //Size
            tl_ItemDescription1.Text = pageItems[0].getItemDescription().ToString();  //Description

            //===Adding=Item=Two===//
            tl_ItemName2.Text = pageItems[1].getItemName();  //Name
            tl_ItemPrice2.Text = "$" + pageItems[1].getItemPrice().ToString();  //Price
            nud_ItemStock2.Value = pageItems[1].getItemStock();  //Stock
            tl_ItemWeight2.Text = pageItems[1].getItemWeight().ToString();  //Weight
            tl_ItemSize2.Text = pageItems[1].getItemSize().ToString();  //Size
            tl_ItemDescription2.Text = pageItems[1].getItemDescription().ToString();  //Description

            //===Adding=Item=Three===//
            tl_ItemName3.Text = pageItems[2].getItemName();  //Name
            tl_ItemPrice3.Text = "$" + pageItems[2].getItemPrice().ToString();  //Price
            nud_ItemStock3.Value = pageItems[2].getItemStock();  //Stock
            tl_ItemWeight3.Text = pageItems[2].getItemWeight().ToString();  //Weight
            tl_ItemSize3.Text = pageItems[2].getItemSize().ToString();  //Size
            tl_ItemDescription3.Text = pageItems[2].getItemDescription().ToString();  //Description

            //===Adding=Item=Four===//
            tl_ItemName4.Text = pageItems[3].getItemName();  //Name
            tl_ItemPrice4.Text = "$" + pageItems[3].getItemPrice().ToString();  //Price
            nud_ItemStock4.Value = pageItems[3].getItemStock();  //Stock
            tl_ItemWeight4.Text = pageItems[3].getItemWeight().ToString();  //Weight
            tl_ItemSize4.Text = pageItems[3].getItemSize().ToString();  //Size
            tl_ItemDescription4.Text = pageItems[3].getItemDescription().ToString();  //Description
        }

        //===========================//
        //=======Updating=Form=======//
        //===========================//
        public void updatePage()
        {
            //===Getting=Updates===//
            Program.inventory.restockInventoryItem(Program.pageList[0], (int) (nud_ItemStock1.Value - Program.pageList[0].getItemStock()));  //Saving stock of item 1
            Program.inventory.restockInventoryItem(Program.pageList[1], (int)(nud_ItemStock2.Value - Program.pageList[1].getItemStock()));  //Saving stock of item 2
            Program.inventory.restockInventoryItem(Program.pageList[2], (int)(nud_ItemStock3.Value - Program.pageList[2].getItemStock()));  //Saving stock of item 3
            Program.inventory.restockInventoryItem(Program.pageList[3], (int)(nud_ItemStock4.Value - Program.pageList[3].getItemStock()));  //Saving stock of item 4
            Program.pageList = Program.inventory.pullForMenu(currentPage);  //Pulling Page
            Program.inventory.generatePageCount();  //Generating Page Number

            //===Updating=Pages===//
            setPagesCountText();  //Updating pages text
            printMenuPage(Program.pageList);  //Updating Page
        }

        //=================================//
        //=======Refresh=Page=Button=======//
        //=================================//
        private void btn_RefreshPage_Click(object sender, EventArgs e)
        {
            updatePage();  //Calling Update Page Method
        }

        //==================================//
        //=======Edit=Item=One=Button=======//
        //==================================//
        private void btn_ItemEdit1_Click(object sender, EventArgs e)
        {
            editItem(0);  //Sending Data to be Edited through Method
        }

        //==================================//
        //=======Edit=Item=Two=Button=======//
        //==================================//
        private void btn_ItemEdit2_Click(object sender, EventArgs e)
        {
            editItem(1);  //Sending Data to be Edited through Method
        }

        //====================================//
        //=======Edit=Item=Three=Button=======//
        //====================================//
        private void btn_ItemEdit3_Click(object sender, EventArgs e)
        {
            editItem(2);  //Sending Data to be Edited through Method
        }

        //===================================//
        //=======Edit=Item=Four=Button=======//
        //===================================//
        private void btn_ItemEdit4_Click(object sender, EventArgs e)
        {
            editItem(3);  //Sending Data to be Edited through Method
        }

        //=======================//
        //=======Edit=Item=======//
        //=======================//
        private void editItem(int buttonNumber)
        {
            if (Program.pageList[buttonNumber].getItemName().CompareTo("") != 0 &&
                Program.pageList[buttonNumber].getItemPrice() != 0 &&
                Program.pageList[buttonNumber].getItemStock() != 0 &&
                Program.pageList[buttonNumber].getItemWeight() != 0 &&
                Program.pageList[buttonNumber].getItemSize() != 0 &&
                Program.pageList[buttonNumber].getItemDescription().CompareTo("") != 0)  //If everything is null, then it is a nonexistent item
            {
                f_EditItem edit = new f_EditItem(Program.pageList[buttonNumber], true);  //Instantiating an edit window for the item
                edit.Show();  //Opening edit window
            }
        }

        //====================================//
        //=======Remove=Item=One=Button=======//
        //====================================//
        private void btn_ItemDelete1_Click(object sender, EventArgs e)
        {
            removeItem(0);
        }

        //====================================//
        //=======Remove=Item=Two=Button=======//
        //====================================//
        private void btn_ItemDelete2_Click(object sender, EventArgs e)
        {
            removeItem(1);
        }

        //======================================//
        //=======Remove=Item=Three=Button=======//
        //======================================//
        private void btn_ItemDelete3_Click(object sender, EventArgs e)
        {
            removeItem(2);
        }

        //=====================================//
        //=======Remove=Item=Four=Button=======//
        //=====================================//
        private void btn_ItemDelete4_Click(object sender, EventArgs e)
        {
            removeItem(3);
        }

        //=========================//
        //=======Remove=Item=======//
        //=========================//
        private void removeItem(int buttonNumber)
        {
            if (Program.pageList[buttonNumber].getItemName().CompareTo("") != 0 &&
                Program.pageList[buttonNumber].getItemPrice() != 0 &&
                Program.pageList[buttonNumber].getItemStock() != 0 &&
                Program.pageList[buttonNumber].getItemWeight() != 0 &&
                Program.pageList[buttonNumber].getItemSize() != 0 &&
                Program.pageList[buttonNumber].getItemDescription().CompareTo("") != 0)  //If everything is null, then it is a nonexistent item
            {
                Program.inventory.removeInventoryItem(Program.pageList[buttonNumber]);  //Removing Object from List
                updatePage();  //Updating Page
            }
        }

        //==============================//
        //=======Next=Page=Button=======//
        //==============================//
        private void btn_NextPage_Click(object sender, EventArgs e)
        {
            if (currentPage <= Program.inventory.getPageCount() - 1)  //If can go to next page
            {
                currentPage++;  //Incrementing Up
                updatePage();  //Updating Page
            }
        }

        //==================================//
        //=======Previous=Page=Button=======//
        //==================================//
        private void btn_PreviousPage_Click(object sender, EventArgs e)
        {
            if (currentPage >= 2)  //If can go to next page
            {
                currentPage--;  //Incrementing Down
                updatePage();  //Updating Page
            }
        }

        //=============================//
        //=======Add=Item=Button=======//
        //=============================//
        private void btn_AddItem_Click(object sender, EventArgs e)
        {
            f_EditItem edit = new f_EditItem(null, false);  //Instantiating an edit window for the item
            edit.Show();  //Opening edit window
        }

        //==========================//
        //=======Sort=Buttons=======//
        //==========================//
        //=======Alphabetically=======//
        private void rb_Alphabetics_CheckedChanged(object sender, EventArgs e)
        {
            Program.inventory.sortAlphabetically();  //Calling sort function
            updatePage();  //Updating page
        }

        //=======Highest=Price=======//
        private void rb_SortHighPrice_CheckedChanged(object sender, EventArgs e)
        {
            Program.inventory.sortByPrice(false);  //Calling sort function
            updatePage();  //Updating page
        }

        //=======Lowest=Price=======//
        private void rb_SortLowPrice_CheckedChanged(object sender, EventArgs e)
        {
            Program.inventory.sortByPrice(true);  //Calling sort function
            updatePage();  //Updating page
        }

        //=======Highest=Stock=======//
        private void rb_HighestStock_CheckedChanged(object sender, EventArgs e)
        {
            Program.inventory.sortByStock(false);  //Calling sort function
            updatePage();  //Updating page
        }

        //=======Lowest=Stock=======//
        private void rb_LowestStock_CheckedChanged(object sender, EventArgs e)
        {
            Program.inventory.sortByStock(true);  //Calling sort function
            updatePage();  //Updating page
        }

        //=======Lightest=======//
        private void rb_Lightest_CheckedChanged(object sender, EventArgs e)
        {
            Program.inventory.sortByWeight(true);  //Calling sort function
            updatePage();  //Updating page
        }

        //=======Heaviest=======//
        private void rb_Heaviest_CheckedChanged(object sender, EventArgs e)
        {
            Program.inventory.sortByWeight(false);  //Calling sort function
            updatePage();  //Updating page
        }

        //=======Smallest=======//
        private void rb_Smallest_CheckedChanged(object sender, EventArgs e)
        {
            Program.inventory.sortBySize(true);  //Calling sort function
            updatePage();  //Updating page
        }

        //=======Biggest=======//
        private void rb_Biggest_CheckedChanged(object sender, EventArgs e)
        {
            Program.inventory.sortBySize(false);  //Calling sort function
            updatePage();  //Updating page
        }
    }
}