﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone_4
{
    public class InventoryItemComparer : IComparer<InventoryItem>
	{
		//============================//
		//=======ComparisonType=======//
		//============================//
		//=======Enum=======//
		public enum ComparisonType
		{
            itemName = 1, itemPrice, itemStock, itemWeight, itemSize  //Storing different types of comparison
		}

		//=======Getter=&=Setter=======//
		public ComparisonType ComparisonMethod
		{
			set;
			get;
		}

		//=====================//
		//=======Compare=======//
		//=====================//
		public int Compare(InventoryItem ii1, InventoryItem ii2)
		{
			return ii1.CompareTo(ii2, ComparisonMethod);  //Using comparing method
		}
	}
}